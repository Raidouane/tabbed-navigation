﻿using System;

using Xamarin.Forms;

namespace TabbedNavigation
{
    public class InterestsTab : ContentPage
    {
        public InterestsTab()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

